<?php

use Illuminate\Database\Seeder;

use App\Models\office;

class OfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => random_int(1, 100),
                'name' => 'Your Office',
                'ordinal' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        office::insert($data);
    }
}
