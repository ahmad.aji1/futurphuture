<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// WEBSITE
Route::group([
    'namespace' => 'Web'
], function () {
    Route::get('/', 'HomeController@index')->name('web.home');
    Route::post('/tell-me', 'HomeController@tell_me')->name('web.tell_me');
    Route::post('/download-cheatsheet', 'HomeController@request_download')->name('web.request_download');
    Route::get('/sfk-full-cheat-sheet', 'HomeController@view_cheatsheet')->name('web.view_cheatsheet');
});

// Route::get('/', function () {
//     return view('welcome');
// });

// ADMIN
Route::group([
    'prefix' => env('ADMIN_DIR'),
    'namespace' => 'Admin'
], function () {
    if (env('ADMIN_CMS', false) == true) {
        Route::group(['namespace' => 'Core'], function () {
            // AUTH
            Route::get('/login', 'AuthController@login')->name('admin.login');
            Route::post('/login-auth', 'AuthController@login_auth')->name('admin.login.auth');
            Route::get('/logout', 'AuthController@logout')->name('admin.logout');
            Route::get('/logout-all', 'AuthController@logout_all')->name('admin.logout.all');
        });

        // NEED AUTH
        Route::group(['middleware' => 'auth.admin'], function () {

            ### SIORENSYS CORE (PLEASE DO NOT MODIFY THE CODE BELOW, UNLESS YOU UNDERSTAND WHAT YOU ARE DOING) ###
            Route::group(['namespace' => 'Core'], function () {
                // NOTIFICATION
                Route::group(['prefix' => 'notification'], function () {
                    Route::get('/', 'NotificationController@index')->name('admin.notif.list');
                    Route::get('/get-data', 'NotificationController@get_data')->name('admin.notif.get_data');
                    Route::get('/open/{id}', 'NotificationController@open')->name('admin.notif.open');
                    Route::get('/get-data/bar', 'NotificationController@get_notif')->name('admin.notif');
                });

                // CONFIG APP
                Route::match(['get', 'post'], '/config', 'ConfigController@index')->name('admin.config');

                // CHANGE COUNTRY
                Route::get('change-country/{alias}', 'AdminController@change_country')->name('admin.change_country');

                // CHANGE LANGUAGE
                Route::get('change-language/{alias}', 'AdminController@change_language')->name('admin.change_language');

                // PROFILE
                Route::match(['get', 'post'], '/profile', 'AdminController@profile')->name('admin.profile');

                // CHANGE PASSWORD
                Route::post('/change-password', 'AdminController@change_password')->name('admin.change_password');

                // SYSTEM LOGS
                Route::group(['prefix' => 'system-logs'], function () {
                    Route::get('/', 'SystemLogController@index')->name('admin.system_logs');
                    Route::get('/get-data', 'SystemLogController@get_data')->name('admin.system_logs.get_data');
                    Route::get('/{id}', 'SystemLogController@view')->name('admin.system_logs.view');
                });

                // MODULE
                Route::group(['prefix' => 'module'], function () {
                    Route::get('/', 'ModuleController@index')->name('admin.module');
                    Route::get('/get-data', 'ModuleController@get_data')->name('admin.module.get_data');
                    Route::get('/create', 'ModuleController@create')->name('admin.module.create');
                    Route::post('/store', 'ModuleController@store')->name('admin.module.store');
                    Route::get('/edit/{id}', 'ModuleController@edit')->name('admin.module.edit');
                    Route::post('/update/{id}', 'ModuleController@update')->name('admin.module.update');
                    Route::post('/delete', 'ModuleController@delete')->name('admin.module.delete');
                    Route::get('/deleted-data', 'ModuleController@deleted_data')->name('admin.module.deleted_data');
                    Route::get('/get-deleted-data', 'ModuleController@get_deleted_data')->name('admin.module.get_deleted_data');
                    Route::post('/restore', 'ModuleController@restore')->name('admin.module.restore');
                });

                // MODULE RULES
                Route::group(['prefix' => 'rules'], function () {
                    Route::get('/', 'ModuleRuleController@index')->name('admin.module_rule');
                    Route::get('/get-data', 'ModuleRuleController@get_data')->name('admin.module_rule.get_data');
                    Route::get('/create', 'ModuleRuleController@create')->name('admin.module_rule.create');
                    Route::post('/store', 'ModuleRuleController@store')->name('admin.module_rule.store');
                    Route::get('/edit/{id}', 'ModuleRuleController@edit')->name('admin.module_rule.edit');
                    Route::post('/update/{id}', 'ModuleRuleController@update')->name('admin.module_rule.update');
                    Route::post('/delete', 'ModuleRuleController@delete')->name('admin.module_rule.delete');
                    Route::get('/deleted-data', 'ModuleRuleController@deleted_data')->name('admin.module_rule.deleted_data');
                    Route::get('/get-deleted-data', 'ModuleRuleController@get_deleted_data')->name('admin.module_rule.get_deleted_data');
                    Route::post('/restore', 'ModuleRuleController@restore')->name('admin.module_rule.restore');
                });

                // PHRASE
                Route::group(['prefix' => 'phrase'], function () {
                    Route::get('/', 'PhraseController@index')->name('admin.phrase');
                    Route::get('/get-data', 'PhraseController@get_data')->name('admin.phrase.get_data');
                    Route::get('/create', 'PhraseController@create')->name('admin.phrase.create');
                    Route::post('/store', 'PhraseController@store')->name('admin.phrase.store');
                    Route::get('/edit/{id}', 'PhraseController@edit')->name('admin.phrase.edit');
                    Route::post('/update/{id}', 'PhraseController@update')->name('admin.phrase.update');
                    Route::post('/delete', 'PhraseController@delete')->name('admin.phrase.delete');
                    Route::get('/deleted-data', 'PhraseController@deleted_data')->name('admin.phrase.deleted_data');
                    Route::get('/get-deleted-data', 'PhraseController@get_deleted_data')->name('admin.phrase.get_deleted_data');
                    Route::post('/restore', 'PhraseController@restore')->name('admin.phrase.restore');
                });

                // OFFICE
                Route::group(['prefix' => 'office'], function () {
                    Route::get('/', 'OfficeController@index')->name('admin.office');
                    Route::get('/get-data', 'OfficeController@get_data')->name('admin.office.get_data');
                    Route::get('/create', 'OfficeController@create')->name('admin.office.create');
                    Route::post('/store', 'OfficeController@store')->name('admin.office.store');
                    Route::get('/edit/{id}', 'OfficeController@edit')->name('admin.office.edit');
                    Route::post('/update/{id}', 'OfficeController@update')->name('admin.office.update');
                    Route::post('/delete', 'OfficeController@delete')->name('admin.office.delete');
                    Route::get('/deleted-data', 'OfficeController@deleted_data')->name('admin.office.deleted_data');
                    Route::get('/get-deleted-data', 'OfficeController@get_deleted_data')->name('admin.office.get_deleted_data');
                    Route::post('/restore', 'OfficeController@restore')->name('admin.office.restore');
                    Route::post('/sorting', 'OfficeController@sorting')->name('admin.office.sorting');

                    // BRANCH
                    Route::group(['prefix' => 'branch/{office_id}'], function () {
                        Route::get('/', 'OfficeBranchController@index')->name('admin.office_branch');
                        Route::get('/get-data', 'OfficeBranchController@get_data')->name('admin.office_branch.get_data');
                        Route::get('/create', 'OfficeBranchController@create')->name('admin.office_branch.create');
                        Route::post('/store', 'OfficeBranchController@store')->name('admin.office_branch.store');
                        Route::get('/edit/{id}', 'OfficeBranchController@edit')->name('admin.office_branch.edit');
                        Route::post('/update/{id}', 'OfficeBranchController@update')->name('admin.office_branch.update');
                        Route::post('/delete', 'OfficeBranchController@delete')->name('admin.office_branch.delete');
                        Route::get('/deleted-data', 'OfficeBranchController@deleted_data')->name('admin.office_branch.deleted_data');
                        Route::get('/get-deleted-data', 'OfficeBranchController@get_deleted_data')->name('admin.office_branch.get_deleted_data');
                        Route::post('/restore', 'OfficeBranchController@restore')->name('admin.office_branch.restore');
                        Route::post('/sorting', 'OfficeBranchController@sorting')->name('admin.office_branch.sorting');
                    });
                });

                // ADMIN GROUP
                Route::group(['prefix' => 'group'], function () {
                    Route::get('/', 'AdminGroupController@index')->name('admin.group');
                    Route::get('/get-data', 'AdminGroupController@get_data')->name('admin.group.get_data');
                    Route::get('/create', 'AdminGroupController@create')->name('admin.group.create');
                    Route::post('/store', 'AdminGroupController@store')->name('admin.group.store');
                    Route::get('/edit/{id}', 'AdminGroupController@edit')->name('admin.group.edit');
                    Route::post('/update/{id}', 'AdminGroupController@update')->name('admin.group.update');
                    Route::post('/delete', 'AdminGroupController@delete')->name('admin.group.delete');
                    Route::get('/deleted-data', 'AdminGroupController@deleted_data')->name('admin.group.deleted_data');
                    Route::get('/get-deleted-data', 'AdminGroupController@get_deleted_data')->name('admin.group.get_deleted_data');
                    Route::post('/restore', 'AdminGroupController@restore')->name('admin.group.restore');
                });

                // ADMIN
                Route::group(['prefix' => 'administrator'], function () {
                    Route::get('/', 'AdminController@index')->name('admin.user_admin');
                    Route::get('/get-data', 'AdminController@get_data')->name('admin.user_admin.get_data');
                    Route::get('/create', 'AdminController@create')->name('admin.user_admin.create');
                    Route::post('/store', 'AdminController@store')->name('admin.user_admin.store');
                    Route::get('/edit/{id}', 'AdminController@edit')->name('admin.user_admin.edit');
                    Route::post('/update/{id}', 'AdminController@update')->name('admin.user_admin.update');
                    Route::post('/reset-password/{id}', 'AdminController@reset_password')->name('admin.user_admin.reset_password');
                    Route::post('/delete', 'AdminController@delete')->name('admin.user_admin.delete');
                    Route::get('/deleted-data', 'AdminController@deleted_data')->name('admin.user_admin.deleted_data');
                    Route::get('/get-deleted-data', 'AdminController@get_deleted_data')->name('admin.user_admin.get_deleted_data');
                    Route::post('/restore', 'AdminController@restore')->name('admin.user_admin.restore');
                });

                // COUNTRY
                Route::group(['prefix' => 'country'], function () {
                    Route::get('/', 'CountryController@index')->name('admin.country');
                    Route::get('/get-data', 'CountryController@get_data')->name('admin.country.get_data');
                    Route::get('/create', 'CountryController@create')->name('admin.country.create');
                    Route::post('/store', 'CountryController@store')->name('admin.country.store');
                    Route::get('/edit/{id}', 'CountryController@edit')->name('admin.country.edit');
                    Route::post('/update/{id}', 'CountryController@update')->name('admin.country.update');
                    Route::post('/delete', 'CountryController@delete')->name('admin.country.delete');
                    Route::get('/deleted-data', 'CountryController@deleted_data')->name('admin.country.deleted_data');
                    Route::get('/get-deleted-data', 'CountryController@get_deleted_data')->name('admin.country.get_deleted_data');
                    Route::post('/restore', 'CountryController@restore')->name('admin.country.restore');

                    // LANGUAGE
                    Route::group(['prefix' => '{parent_id}/language'], function () {
                        Route::get('/', 'LanguageController@index')->name('admin.language');
                        Route::get('/get-data', 'LanguageController@get_data')->name('admin.language.get_data');
                        Route::get('/create', 'LanguageController@create')->name('admin.language.create');
                        Route::post('/store', 'LanguageController@store')->name('admin.language.store');
                        Route::get('/edit/{id}', 'LanguageController@edit')->name('admin.language.edit');
                        Route::post('/update/{id}', 'LanguageController@update')->name('admin.language.update');
                        Route::post('/delete', 'LanguageController@delete')->name('admin.language.delete');
                        Route::get('/deleted-data', 'LanguageController@deleted_data')->name('admin.language.deleted_data');
                        Route::get('/get-deleted-data', 'LanguageController@get_deleted_data')->name('admin.language.get_deleted_data');
                        Route::post('/restore', 'LanguageController@restore')->name('admin.language.restore');
                        Route::post('/sorting', 'LanguageController@sorting')->name('admin.language.sorting');
                        Route::get('/dictionary/{id}', 'LanguageController@dictionary')->name('admin.language.dictionary');
                        Route::post('/dictionary/{id}/save', 'LanguageController@dictionary_save')->name('admin.language.dictionary.save');
                    });
                });
            });
            ### SIORENSYS CORE - END ###

            /**
             * ******************* ADD ANOTHER CUSTOM ROUTES BELOW *******************
             */

            // HOME
            Route::get('/', 'HomeController@index')->name('admin.home');

            // VISITOR / TECH SUITE INTEREST
            Route::group(['prefix' => 'visitor'], function () {
                Route::get('/', 'VisitorController@index')->name('admin.visitor');
                Route::get('/get-data', 'VisitorController@get_data')->name('admin.visitor.get_data');
                Route::get('/view/{id}', 'VisitorController@view')->name('admin.visitor.view');
                Route::get('/export-excel', 'VisitorController@export_excel')->name('admin.visitor.export_excel');
            });

            // DOWNLOAD CHEAT SHEET
            Route::group(['prefix' => 'download-cheatsheet'], function () {
                Route::get('/', 'DownloadCheatsheetController@index')->name('admin.download_cheatsheet');
                Route::get('/get-data', 'DownloadCheatsheetController@get_data')->name('admin.download_cheatsheet.get_data');
                Route::get('/view/{id}', 'DownloadCheatsheetController@view')->name('admin.download_cheatsheet.view');
                Route::get('/export-excel', 'DownloadCheatsheetController@export_excel')->name('admin.download_cheatsheet.export_excel');
            });
        });
    }
});

// HELPER
Route::group(['prefix' => 'helper'], function () {
    Route::post('/filter-district', 'HelperController@filter_district')->name('helper.filter_district');
    Route::post('/filter-sub-district', 'HelperController@filter_sub_district')->name('helper.filter_sub_district');
    Route::post('/filter-village', 'HelperController@filter_village')->name('helper.filter_village');
    Route::post('/filter-postal-code', 'HelperController@filter_postal_code')->name('helper.filter_postal_code');
});

// CRON
Route::group(['prefix' => 'cron'], function () {
    Route::get('/cc-bill-payment-reminder', 'CronController@cc_bill_payment_reminder');
});

// DEVELOPMENT TESTER
Route::group(['prefix' => 'dev'], function () {
    // SANDBOX
    Route::get('/sandbox', 'DevController@sandbox');

    // NEED AUTH
    Route::group(['middleware' => 'auth.admin'], function () {
        // CHEATSHEET FORM
        Route::get('/cheatsheet-form', 'DevController@cheatsheet_form')->name('dev.cheatsheet_form');

        // CRYPT TOOLS
        Route::match(['get', 'post'], '/encrypt', 'DevController@encrypt')->name('dev.encrypt');
        Route::match(['get', 'post'], '/decrypt', 'DevController@decrypt')->name('dev.decrypt');

        // EMAIL
        Route::group(['prefix' => 'email'], function () {
            // Send Email using SMTP - sample: "{URL}/dev/email?send=true&email=username@domain.com"
            // Preview Email - sample: "{URL}/dev/email"
            Route::get('/', 'DevController@email_send');
        });
    });
});
