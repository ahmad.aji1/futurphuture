function show_popup_know(selected_value){
	$('#visitor_interest').val(selected_value);
	$('#know_something').html(selected_value);
	$('.popup_cat').show();
	var popup = document.getElementById('popup_cat').offsetTop
	window.scrollTo(popup, 0)
}
function show_popup_hole(){
	$('#hole_popup').show();
}
function hide_popup(){
	$('.popup_cat, .popup_cs').hide();
}


$(document).ready(function(){
	$('.cs_btn').click(function(){
		$('.popup_cs').show();
	})
	var mobile_query = window.matchMedia("(max-width: 599px)")

	if(!mobile_query.matches){
		$('.poster_box').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true
		});
	}
	
	$('#menu_btn').click(function(){
		$('.main_menu').show();
	});
	$('.main_menu .menu_box .menu_btn').click(function(){
		$('.main_menu').hide();
	});
	// var menubox_click = document.getElementById('menu_box')
	// var isMenuClicked = false
	// menubox_click.addEventListener('click', () => {
	// 	if (!isMenuClicked) {
	// 		$('.main_menu').show();
	// 	} else {
	// 		$('.main_menu').hide();
	// 	}
	// 	isMenuClicked = !isMenuClicked
	// })
})

$(window).bind('load', function(){
	$('.video_wrapper').fadeIn();
	setTimeout(function(){
		$('.logo').fadeIn()}, 500)
})