<!DOCTYPE html>
<html>
<head>
	<title>{!! $subject !!}</title>
</head>
<body style="padding:0;margin:0 auto;">
	<table style="width: 100%;max-width: 600px;margin:0 auto;font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;border:1px solid #000;">
		<thead>
			<tr>
				<th colspan="6" style="padding: 20px 0 0;background: #fff;"><img style="width: 100px" src="{{ asset('web/images/sfk_text.png') }}"></th>
			</tr>
		</thead>
		<tbody style="background: #fff;">
			<tr>
				<td colspan="6" style="color:#000;background:#fff;font-family: Arial, Helvetica, sans-serif;padding:20px;">
					<h2 style="color:#000;">Hi {!! $data->name !!},</h2>
					<p>You really want to cheat, don't you 😜 ... here you go ...</p>
					<div style="text-align: center;">
						<a href="{{ $action_url }}" style="background: #000;color:#fff;text-decoration: none;display: inline-block;padding:10px 20px;">Download Cheat Sheet</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>