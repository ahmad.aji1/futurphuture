@php
// Libraries
use App\Libraries\Helper;
@endphp

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="target-densitydpi=device-dpi; width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="HandheldFriendly" content="true" />
    <link rel="icon" href="{{ asset($global_config->app_favicon) }}" />
    <title>@if(View::hasSection('title'))@yield('title') | @endif{!! $global_config->og_title !!}</title>
    <meta name="description" content="{!! $global_config->meta_description !!}">
    <meta name="keywords" content="{!! str_replace(',', ', ', $global_config->meta_keywords) !!}">

    @if(View::hasSection('open_graph'))
    @yield('open_graph')
    @else
    {{-- DEFAULT OPEN GRAPH --}}
    @if (isset($global_config->og_type))
    <meta property="og:type" content="{!! $global_config->og_type !!}" />
    <meta property="og:site_name" content="{!! $global_config->og_site_name !!}" />
    <meta property="og:title"
        content="@if(View::hasSection('title'))@yield('title')@else{!! $global_config->og_title !!}@endif" />
    <meta property="og:image" content="{{ asset($global_config->og_image) }}" />
    <meta property="og:description" content="{!! $global_config->og_description !!}" />
    <meta property="og:url" content="{{ Helper::get_url() }}" />

    @if ($global_config->fb_app_id)
    <meta property="fb:app_id" content="{!! $global_config->fb_app_id !!}" />
    @endif

    <meta property="twitter:card" content="{!! $global_config->twitter_card !!}" />
    @if ($global_config->twitter_site)
    <meta property="twitter:site" content="{!! $global_config->twitter_site !!}" />
    @endif
    @if ($global_config->twitter_site_id)
    <meta property="twitter:site:id" content="{!! $global_config->twitter_site_id !!}" />
    @endif
    @if ($global_config->twitter_creator)
    <meta property="twitter:creator" content="{!! $global_config->twitter_creator !!}" />
    @endif
    @if ($global_config->twitter_creator_id)
    <meta property="twitter:creator:id" content="{!! $global_config->twitter_creator_id !!}" />
    @endif
    @endif
    @endif
    <!-- -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web/fonts/stylesheet.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/fullpage.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/slick-theme.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/e_main.css') }}?v=1.4" />
    <link rel="stylesheet" type="text/css" href="{{ asset('web/css/main.css') }}" />
    <script type="text/javascript" src="{{ asset('web/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/wow.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/scrolloverflow.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/fullpage.js') }}"></script>
    <style>
    /* Style for our header texts
      * --------------------------------------- */
    h1 {
        font-size: 5em;
        font-family: arial, helvetica;
        color: #fff;
        margin: 0;
        padding: 40px 0 0 0;
    }

    .intro p {
        color: #fff;
        padding: 40px 0 0 0;
    }

    /* Centered texts in each section
	    * --------------------------------------- */
    .section {
        text-align: center;
    }

    /* Bottom menu
	    * --------------------------------------- */
    #infoMenu li a {
        color: #fff;
    }
    </style>

    <!--[if IE]>
      <script type="text/javascript">
        var console = { log: function () {} };
      </script>
    <![endif]-->

    @if (env('RECAPTCHA_SECRET_KEY_PUBLIC'))
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    @endif

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VJ69C0Q2CP"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VJ69C0Q2CP');
    </script>
    <script>
        function startVideoHandle() {
            document.getElementById('video-section0').play();
            document.getElementById('playVideo').style.display = 'none'
        }

        function pauseVideoHandle() {
            document.getElementById('video-section0').pause();
            document.getElementById('playVideo').style.display = 'flex'
        }
        
    </script>
</head>

<body>
    <style type="text/css">
    @media only screen and (max-width: 767px) {
        section .category_wrapper {
            position: static;
            height: auto;
            width: 100%;
        }

        section .category_wrapper .category_col {
            width: 100%;
            float: none;
            height: auto;
        }

        section .category_wrapper .cat_one,
        section .category_wrapper .cat_two,
        section .category_wrapper .cat_three,
        section .category_wrapper .cat_four,
        section .category_wrapper .cat_five,
        section .category_wrapper .cat_six,
        section .category_wrapper .cat_seven,
        section .category_wrapper .cat_eight {
            height: 300px;
        }
    }
    </style>
    <audio src="{{ asset('web/audio/horse.ogg') }}" preload="auto" id="audio" style="display: none"></audio>
    <div class="loader">
        <div class="loader_box">
            <div class="loader_dot"></div>
            <div class="loader_dot"></div>
            <div class="loader_dot"></div>
            <div class="loader_dot"></div>
            <div class="loader_dot"></div>
            <div class="loader_dot"></div>
            <div class="loader_dot"></div>
            <div class="loader_text"></div>
        </div>
    </div>
    <div class="menu_box_front" style="display: none">
        <div class="menu_btn" id="menu_btn"></div>
    </div>
    <div class="main_menu">
        <div class="menu_box">
            <div class="menu_btn"></div>
        </div>
        <ul>
            <li><a href="#" id="whatwedo" class="active">... what we do</a></li>
            <li><a href="#" id="ourtechsuite">... our tech suite</a></li>
            <li><a href="#" id="yourhole">... your rabbit hole</a></li>
            <li><a href="#" id="tfdyk">... WTF do you know?</a></li>
        </ul>
    </div>
    <div class="popup_cat" id="popup_cat">
        <div class="pcat_box">
            <h3>I Want To Know <b id="know_something">Something</b></h3>
            <div class="form_box">
                <input type="text" placeholder="[FIRST] [LAST] NAME" id="visitor_name" />
            </div>
            <div class="form_box">
                <input type="email" placeholder="Email Address" id="visitor_email" />
            </div>
            <div class="form_box">
                <input type="text" placeholder="City" id="visitor_address" />
            </div>
            <input type="hidden" id="visitor_interest">
            @if (env('RECAPTCHA_SECRET_KEY_PUBLIC'))
            <center>
                <div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_SITE_KEY_PUBLIC') }}"></div>
            </center>
            @endif
            <button class="black_btn" style="margin-top:20px;" onclick="tell_me()">Tell Me Now</button>
            <a href="#" class="close_btn" onclick="hide_popup();" style="right: 20px;">X</a>
        </div>
    </div>
    <section id="fullpage">
        <div class="section" id="section0">
            <div class="video_wrapper">
                <video id='video-section0' src="{{ asset('videos/dummy_video.mp4') }}" style="width: 100%; height: 100vh;" onclick="pauseVideoHandle()"></video>
                <div id="playVideo" onclick="startVideoHandle()" style="position: absolute; z-index: 2; width: 100%; height: 90vh; background-color: #00000055; border: none; display: flex; justify-content: center; align-items: center;">
                    <img src="{{ asset('web/images/arrow_right.png') }}" style="width: 100px; height: 100px" />
                </div>
            </div>
            <!-- <h1 class="logo"><img src="{{ asset('web/images/sfk_text.png') }}" /></h1> -->
            <a href="#" id="arrow_btn" class="scroll_btn"><img src="{{ asset('web/images/arrow.png') }}" /></a>
        </div>
        <div class="section" id="section1">
            <div class="container">
                <h3>Who Are You</h3>
                <div class="photo_wrapper">
                    <div class="photo_box">
                        <div class="photo_img color_one">
                            <img src="{{ asset('web/images/sfk1.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Growth <br />Driver</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_two">
                            <img src="{{ asset('web/images/sfk2.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Capability <br />Architect</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_three">
                            <img src="{{ asset('web/images/sfk3.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Reputation <br />Manager</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_four">
                            <img src="{{ asset('web/images/sfk4.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Game <br />Changer</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_five">
                            <img src="{{ asset('web/images/sfk5.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Talent <br />Provocateur</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_six">
                            <img src="{{ asset('web/images/sfk6.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Story <br />Teller</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_seven">
                            <img src="{{ asset('web/images/sfk7.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Innovation <br />Cadet</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_eight">
                            <img src="{{ asset('web/images/sfk8.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Security <br />Engineer</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_nine">
                            <img src="{{ asset('web/images/sfk9.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>People's <br />Champion</h4>
                        </div>
                    </div>
                    <div class="photo_box">
                        <div class="photo_img color_ten">
                            <img src="{{ asset('web/images/sfk10.png') }}" />
                        </div>
                        <a href="#" class="e_fake_box"></a>
                        <div class="photo_desc white">
                            <h4>Shark <br />Hunter</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section2">
            <div class="container section2">
                <p>Actually we don&#8217;t really care who you are.</p>
                <p>We just care that you beat the odds by</p>
                <img src="{{ asset('web/images/img1.png') }}" />
                <p>than whoever, whatever your opposite is.</p>
            </div>
        </div>
        <section class="section">
            <div class="main-of-content">
                <div class="main-container">
                    <div class="container-page slide-1">
                        <div class="container-content">
                            <h3 class="desktop-google"><b>Google</b></h3>
                            <h3 class="mobile-google"><b>Google</b></h3>
                            <h3>
                                Defines <b>ZERO MOMENT OF TRUTH</b> as the moment when a
                                person searches online.
                            </h3>
                        </div>
                    </div>
                    <div class="container-page slide-2">
                        <div class="main-of-inner">
                            <div class="inner-slide-2">
                                <div class="left-side">
                                    <div class="content-up"></div>
                                    <div class="content-down"></div>
                                </div>
                                <div class="right-side">
                                    <div class="content">
                                        <h3 class="we">WE</h3>
                                        <h3>Define two distinctive moments</h3>
                                        <br />
                                        <br />
                                        <h3><b>SUB-ZERO MOMENT OF TRUTH</b></h3>
                                        <br />
                                        <h3>
                                            ... when a specific cocktail of emotions triggers and
                                            stimulates a person to search.
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-page slide-3">
                        <div class="left-side"></div>
                        <div class="right-side">
                            <div class="up-content"></div>
                            <div class="down-content">
                                <h3><b>ULTIMATE MOMENT OF TRUTH</b></h3>
                                <br />
                                <h3>
                                    ... the moment loyalty transforms into habits <br />which
                                    obviate the need to make choices.
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section"></section>
        <section class="section"></section>
        <section class="section">
            <div class="main-of-content-2">
                <div class="main-container">
                    <div class="container-page slide-4">
                        <div class="left-side">
                            <!-- <h3 class="title"><b>In The Free Market of</b></h3>
                            <br />
                            <h3>Brand Growth</h3>
                            <h3>Corporate Social Responsibility</h3>
                            <h3>Crisis Intervention</h3>
                            <h3>Digital Risk Protection</h3>
                            <h3>Go-To-Market Intelligence</h3>
                            <h3>Influencers' Identification</h3>
                            <h3>Investors and Investment</h3>
                            <h3>Reputation Management</h3>
                            <h3>Social Channel Measurement</h3>
                            <h3>Strategic / Thematic Campaign</h3>
                            <h3>Talent Acquisition</h3> -->
                            <h3>we can help you move from</h3>
                            <br />
                            <h3 class="thinking">thinking what to do</h3>
                            <br />
                            <h3>to</h3>
                            <br />
                            <h3 class="knowing">knowing how to do</h3>
                        </div>

                        <div class="right-side"></div>
                    </div>
                    <div class="container-page slide-5">
                        <div class="left-side"></div>
                        <div class="right-side">
                            <!-- <h3>we can help you move from</h3>
                            <br />
                            <h3 class="thinking">thinking what to do</h3>
                            <br />
                            <h3>to</h3>
                            <br />
                            <h3 class="knowing">knowing how to do</h3> -->

                            <h3 class="title"><b>In The Free Market of</b></h3>
                            <br />
                            <h3>Brand Growth</h3>
                            <h3>Corporate Social Responsibility</h3>
                            <h3>Crisis Intervention</h3>
                            <h3>Digital Risk Protection</h3>
                            <h3>Go-To-Market Intelligence</h3>
                            <h3>Influencers' Identification</h3>
                            <h3>Investors and Investment</h3>
                            <h3>Reputation Management</h3>
                            <h3>Social Channel Measurement</h3>
                            <h3>Strategic / Thematic Campaign</h3>
                            <h3>Talent Acquisition</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section"></section>
        <div class="section" id="section3">
            <div class="container section3-container" id="section3-container">
                <h2>How we do what we do</h2>
                <p>
                    a supercharged hybrid methodology that is built to serve silo
                    purposes or united goals
                </p>
                <div class="cs_wrapper">
                    <div class="cs_box yellow_bg">
                        <h5>Collect</h5>
                        <ul>
                            <li>search engines</li>
                            <li>social networks</li>
                            <li>displays and videos</li>
                            <li>e-commerce and marketplaces</li>
                            <li>user generated sites</li>
                            <li>deep web</li>
                            <li>dark web</li>
                        </ul>
                    </div>
                    <div class="cs_box white blue_bg">
                        <h5>Contextualize</h5>
                        <ul>
                            <li>conversations</li>
                            <li>search aggregation</li>
                            <li>trend spotting</li>
                            <li>cognitive process</li>
                            <li>linguistic analysis</li>
                            <li>sentiments</li>
                            <li>motivation</li>
                            <li>time perspective</li>
                            <li>attitude</li>
                            <li>cognitive biases</li>
                            <li>macro interruptions</li>
                            <li>emo-triggers</li>
                            <li>fact-checks</li>
                            <li>exposure & virality</li>
                            <li>human or bots</li>
                        </ul>
                    </div>
                    <div class="cs_box white purple_bg">
                        <h5>Report</h5>
                        <ul>
                            <li>most relevant metric stack</li>
                            <li>real-time, on-the-go dashboard</li>
                            <li>customable to unique users in a common group</li>
                        </ul>
                    </div>
                </div>
                <div class="cs_btn">Click Here for Cheat Sheet</div>
                <div class="popup_cs">
                    <div class="popup_cs_box">
                        <img src="{{ asset('web/images/cheat-sheet.jpg') }}" />
                        <div class="form_wrapper">
                            <div class="form_box">
                                <input type="text" placeholder="[FIRST] [LAST] NAME" id="request_name"
                                    onkeyup="this.value = this.value.toUpperCase();" />
                            </div>
                            <div class="form_box">
                                <input type="email" placeholder="EMAIL" id="request_email"
                                    onkeyup="this.value = this.value.toUpperCase();" />
                            </div>
                            <div class="button_box">
                                <button class="dl_btn" onclick="request_download()">Download Full Cheat Sheet</button>
                            </div>
                        </div>
                        <div class="close_btn" onclick="hide_popup();">X</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section4">
            <div class="container">
                <div id="section9">
                    <div class="container section9">
                        <p>
                            While we deploy, stack and blend some of the world's most
                            complicated technologies, <br />our approach will always be<br /><span
                                class="purple_bg">SIMPLICITY ...</span>
                        </p>
                        <div class="lc_wrapper">
                            <h4>the love child of</h4>
                            <img class="desktop" src="{{ asset('web/images/img2.png') }}" />
                            <img class="mobile" src="{{ asset('web/images/wps_YyuHz83Utx.png') }}" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section5">
            <div class="container">
                <div class="category_wrapper desktop" id="cat_wrapper_sec5">
                    <div class="category_col">
                        <div class="category_box cat_one">
                            <div class="cat_front">
                                <h3>our baseline TECH SUITE</h3>
                                <button class="black_btn" onclick="show_popup_know('ALL');">
                                    I Want To Know ALL
                                </button>
                            </div>
                        </div>
                        <div class="category_box cat_two">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo3.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Safeguard your investment by pitting ambitious language
                                            and inspiring propositions against realities By sniffing
                                            out hidden risks and minimize your failure rate by
                                            "re-introducing" your partners to you
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('KUANTICO');">
                                            I Want To Know KUANTICO
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_three">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo2.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Extract and syndicate user-generated content from
                                            ratings and review platforms to increase conversion,
                                            improve products and services, and boost customer
                                            satisfaction
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('RONUT');">
                                            I Want To Know RONUT
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category_col">
                        <div class="category_box cat_four">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo1.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            AI-Powered, Real Time and secured hybrid psycho social
                                            technology that collects, compartmentalizes And
                                            Contextualizes interactions, behaviorial, and
                                            Attitudinal data sets
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('SKYYWALKER');">
                                            I Want To Know SKYYWALKER
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_five">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo4.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Detect and pre-empt potential escalation of high risk
                                            situations, timely appreciation of mishappenings and
                                            identification of negative personalities and their
                                            propensity to participate as dissentious rogues
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('TRIP');">
                                            I Want to Know TRIP
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category_col">
                        <div class="category_box cat_six">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo7.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Community sentiment programming that seeks to understand
                                            what's trending thus helping brands to remain culturally
                                            relevant
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('AMES');">
                                            I Want to Know AMES
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_seven">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo6.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            "live" on-the-go dashboard based on
                                            most-relevant-metric-stack customized for unique users
                                            with common goals
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('SUPA');">
                                            I Want To Know SUPA
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_eight">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <img src="{{ asset('web/images/sfk_logo5.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            leverage on gamification to extract quantitative signals
                                            for prespecified concepts and post-experiences for
                                            growth hacks
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('GEESTACKE');">
                                            I Want To Know GEESTACKE
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="category_wrapper mobile" id="cat_wrapper_sec5">
                    <div class="category_col">
                        <div class="category_box cat_one">
                            <div class="cat_front">
                                <h3>our baseline TECH SUITE</h3>
                                <button class="black_btn" onclick="show_popup_know('ALL');">
                                    I Want To Know ALL
                                </button>
                            </div>
                        </div>
                        <div class="category_box cat_four">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- skywalker -->
                                    <img src="{{ asset('web/images/sfk_logo1.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            AI-Powered, Real Time and secured hybrid psycho social
                                            technology that collects, compartmentalizes And
                                            Contextualizes interactions, behaviorial, and
                                            Attitudinal data sets
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('SKYYWALKER');">
                                            I Want To Know SKYYWALKER
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="category_box cat_six">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- ames -->
                                    <img src="{{ asset('web/images/sfk_logo7.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Community sentiment programming that seeks to understand
                                            what's trending thus helping brands to remain culturally
                                            relevant
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('AMES');">
                                            I Want to Know AMES
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_seven">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- supa -->
                                    <img src="{{ asset('web/images/sfk_logo6.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            "live" on-the-go dashboard based on
                                            most-relevant-metric-stack customized for unique users
                                            with common goals
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('SUPA');">
                                            I Want To Know SUPA
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_three">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- ronut -->
                                    <img src="{{ asset('web/images/sfk_logo2.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Extract and syndicate user-generated content from
                                            ratings and review platforms to increase conversion,
                                            improve products and services, and boost customer
                                            satisfaction
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('RONUT');">
                                            I Want To Know RONUT
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_two">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- kuantico -->
                                    <img src="{{ asset('web/images/sfk_logo3.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Safeguard your investment by pitting ambitious language
                                            and inspiring propositions against realities By sniffing
                                            out hidden risks and minimize your failure rate by
                                            "re-introducing" your partners to you
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('KUANTICO');">
                                            I Want To Know KUANTICO
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category_col">
                        <div class="category_box cat_eight">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- cheesestack -->
                                    <img src="{{ asset('web/images/sfk_logo5.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            leverage on gamification to extract quantitative signals
                                            for prespecified concepts and post-experiences for
                                            growth hacks
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('GEESTACKE');">
                                            I Want To Know GEESTACKE
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category_box cat_five">
                            <div class="cat_box">
                                <div class="cat_front">
                                    <!-- trip -->
                                    <img src="{{ asset('web/images/sfk_logo4.png') }}" />
                                </div>
                                <div class="cat_back">
                                    <div class="cb_box">
                                        <p>
                                            Detect and pre-empt potential escalation of high risk
                                            situations, timely appreciation of mishappenings and
                                            identification of negative personalities and their
                                            propensity to participate as dissentious rogues
                                        </p>
                                        <button class="white_btn" onclick="show_popup_know('TRIP');">
                                            I Want to Know TRIP
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
        <div class="section" id="section6">
            <div class="container">
                <div class="ptwelve_wrap">
                    <div class="ptwelve_box">
                        <div class="ptwelve_left">
                            <h2>Nope...</h2>
                        </div>
                        <div class="ptwelve_right">
                            <p>
                                We don't have a case study to share as we sworn secrecy to all
                                our clients. We do you one better than just showing you other
                                companies' secret. We use what we know to present a POC to you
                                for our next discussion and we will top that by making this
                                exercise <span>PRO BONO.</span>
                            </p>
                            <p>All we need is a problem statement from you.</p>
                        </div>
                        <div class="sticker_box sticker1">
                            <img src="{{ asset('web/images/stiker1.png') }}" />
                        </div>
                        <div class="sticker_box sticker2">
                            <img src="{{ asset('web/images/stiker2.png') }}" />
                        </div>
                        <div class="sticker_box sticker3">
                            <img src="{{ asset('web/images/stiker3.png') }}" />
                        </div>
                        <div class="sticker_box sticker4">
                            <img src="{{ asset('web/images/stiker4.png') }}" />
                        </div>
                        <div class="sticker_box sticker5">
                            <img src="{{ asset('web/images/stiker5.png') }}" />
                        </div>
                        <div class="sticker_box sticker6">
                            <img src="{{ asset('web/images/stiker6.png') }}" />
                        </div>
                        <div class="sticker_box sticker7">
                            <img src="{{ asset('web/images/stiker7.png') }}" />
                        </div>
                        <div class="sticker_box sticker8">
                            <img src="{{ asset('web/images/stiker8.png') }}" />
                        </div>
                        <div class="sticker_box sticker9">
                            <img src="{{ asset('web/images/stiker9.png') }}" />
                        </div>
                    </div>
                    <a href="#" id="contact_btn" class="black_btn">LET'S</a>
                </div>
            </div>
        </div>
        <div class="section" id="section7">
            <div class="container">
                <div class="bg_img"><img src="{{ asset('web/images/bg1.png') }}" /></div>
                <div class="text_top">
                    <!-- <p>
                        You have probably heard similar spiel from Big 3s and 4As. We
                        don't think we are better or worse; we are just different. While
                        we understand you; we don't think like you...
                    </p> -->
                    <p>You have probably heard similar spiel from Big 3s and 4As. <br>We don’t think we are better or
                        worse; we are just different. <br>While we understand you; we don’t think like you … that’s
                        probably how our journey begins ...
                    </p>
                </div>
                <div class="text_bottom">
                    <!-- <p>that's probably how our journey will begin...</p> -->
                    <p>a joint service with</p>
                    <a href="https://www.group-ib.com/" target="blank_"><img
                            src="{{ asset('web/images/group_ib.png') }}"></a>
                    <p> ... a global leader recognized by <span>INTERPOL</span> and <span>Europol</span> in
                        attribution-based threat intelligence, best-in-class threat hunting, fraud prevention, and
                        cybercrime investigations.</p>
                </div>
            </div>
        </div>
        <div class="section" id="section8">
            <div class="container">
                <div class="alice_box">
                    <h3>
                        Alice Says <br />All that Gold <br />Is In That
                        <span onclick="show_popup_hole();">Hole</span>
                    </h3>
                </div>
                <div class="rabbit_box"><img src="{{ asset('web/images/e_rabbit.png') }}" /></div>
                <div class="popup_cat" id="hole_popup"
                    style="max-width: 660px; padding-left: 60px; padding-right: 60px">
                    <div class="pcat_box">
                        <h3>Our competitive edges are ...</h3>
                        <ul>
                            <li>diverse data sources</li>
                            <li>
                                equally competent as a modular solution or integrated
                                capabilities
                            </li>
                            <li>
                                uncovers cognitive bias, emotions, personalities, intentions
                                and actions
                            </li>
                            <li>
                                precision reporting via highly innovative and adapatable
                                Graphical User Interface
                            </li>
                            <li>
                                robust digital risk protection i.e. anti-piracy, anti-fraud
                                and anti-counterfeiting
                            </li>
                            <li>beginner-friendly</li>
                            <li>sensibly priced</li>
                        </ul>
                        <a href="#" class="close_btn" onclick="hide_popup();" style="right: 20px">X</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section9">
            <div class="container">
                <div class="hole_wrap" id="hole_wrap">
                    <h4>Choose Your Rabbit Hole</h4>
                    <div class="contact_wrap">
                        <div class="contact_box malaysia">
                            <div class="contact">
                                <span>Malaysia</span>
                                Choon Ming<br />
                                +60 12 213 9369<br />
                                <a href="mailto:choonming@superfkfuturphuture.io">choonming@futurphuture.io</a>
                            </div>
                        </div>
                        <div class="contact_box indonesia">
                            <div class="contact">
                                <span>Indonesia</span>
                                Jeremy<br />
                                +62 811 9454 546<br />
                                <a href="mailto:jem@futurphuture.io">jem@futurphuture.io</a>
                            </div>
                        </div>
                        <div class="contact_box singapore">
                            <div class="contact">
                                <span>Singapore</span>
                                Nizar<br />
                                +65 9852 3572<br />
                                <a href="mailto:nizar@futurphuture.io">nizar@futurphuture.io</a>
                            </div>
                        </div>
                    </div>

                    <div class="headquarter-container">
                        <fieldset>
                            <legend>HEADQUARTER</legend>
                            <p>Fatmawati Mas, Jl. RS. Fatmawati Raya No.307, Cilandak Bar., Kec. Cilandak, Kota Jakarta Selatan,<br/>Daerah Khusus Ibukota Jakarta 12430</p>
                        </fieldset>                
                    </div>
                    <a href="http://pii.or.id/uploads/dummies.pdf" target="_blank" class="btn-download"><h4 style="margin: 0; letter-spacing: 3.5px;">ANNOUNCEMENT</h4></a>
                </div>
            </div>
        </div>
        <div class="section" id="section10">
            <div class="container">
                <div class="hole_wrap" id="">
                    <h4>what the f**k do you know?</h4>
                    <div class="hole_box">
                        <p>
                            One of the most interesting word in the English language today
                            is the word "F**K" <br />It is a magical word. Just by the
                            variety of its tone, it can express <br />Love, Hate, Pain and
                            Pleasure
                        </p>
                        <p>
                            Now you can find out <br />if you are a F**ker or F**kee
                            <br />by demonstrating your understanding <br />of the emotion
                            behind each "F**k"
                        </p>
                        <a href="{{ env('MICROSITE_URL', '#') }}" target="blank_" class="green_btn">Let's</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section11">
            <div class="container">
                <div class="poster_wrap" id="poster_wrap">
                    <p>come chill with us and our first-loves...</p>
                    <div class="poster_box">
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=6JSvvuSRT5c" target="blank_"><img
                                    src="{{ asset('web/images/tyrant.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=yR_LpahqPfI" target="blank_"><img
                                    src="{{ asset('web/images/shockdoctrine.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=iX8GxLP1FHo" target="blank_"><img
                                    src="{{ asset('web/images/greathack.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=ROVc_47FUD8" target="blank_"><img
                                    src="{{ asset('web/images/split.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=hBLS_OM6Puk" target="blank_"><img
                                    src="{{ asset('web/images/wildwildcountry.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=uaaC57tcci0" target="blank_"><img
                                    src="{{ asset('web/images/socialdilemma.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=edaQ9XwLiXc" target="blank_"><img
                                    src="{{ asset('web/images/mindhunter.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=9LGrzIRR6Hc" target="blank_"><img
                                    src="{{ asset('web/images/searchingforsheela.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=x41SMm-9-i4" target="blank_"><img
                                    src="{{ asset('web/images/Dontfkwiththecats.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=8QnMmpfKWvo" target="blank_"><img
                                    src="{{ asset('web/images/houseofcards.jpg') }}" /></a>
                        </div>
                        <div class="poster_img">
                            <a href="https://www.youtube.com/watch?v=wLoB1eCJ93k" target="blank_"><img
                                    src="{{ asset('web/images/FourHorsemen.jpg') }}" /></a>
                        </div>
                    </div>
                    <p>
                        Click to watch the trailer on YouTube... yes there's a clip for
                        The Shock Doctrine as well.
                    </p>
                </div>
                <div class="copyright">
                    <p>Copyright &copy; 2022 PT LINI IMAJI KREASI EKOSISTEM</p>
                </div>
            </div>
        </div>
    </section>

    <script>
        document.getElementById('video-section0').controls = false;
    </script>

    <script type="text/javascript" src="{{ asset('web/js/scrolloverflow.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/fullpage.js') }}"></script>

    <script type="text/javascript">
    var mobile_query = window.matchMedia("(max-width: 599px)");

    // fungsi adjust query , used di inisiasi fullpage
    function query_adjust(query, callback_onquery, callback_not_onquery) {
        if (query.matches) {
            callback_onquery();
        } else {
            callback_not_onquery();
        }
    }

    //inisiasi fullpage 
    var myFullpage = new fullpage("#fullpage", {
        // param anchor (penanda slide)
        anchors: [
            "slide1",
            "slide2",
            "slide3",
            "firstSection",
            "secondSection",
            "thirdSection",
            "fourthSection",
            "fifthSection",
            "slide4",
            "slide5",
            "slide11",
            "slide12",
            "slide13",
            "slide14",
            "slide15",
            "slide16",
            "slide17",
        ],
        // section collor each slide
        sectionsColor: [
            "#FFFFFF",
            "#FFFFFF",
            "#FFAD47",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FFFFFF",
            "#FF3BE1",
            "#FFFFFF",
            "#FFFFFF",
            "#FFAD47",
        ],
        //scrollOverflow let fullpage have scrollable page inside the slide
        scrollOverflow: true,
        onLeave: function(origin, destination, direction) {
            // console.log(origin.anchor)
            var conditions = {
                ori: origin.anchor,
                des: destination.anchor,
                dir: direction,
            };
            // param previous slide , nextslide
            var {
                ori,
                des,
                dir
            } = conditions;
            //variable nama slide
            const firstSlide = "firstSection";
            const secondSlide = "secondSection";
            const thirdSlide = "thirdSection";
            const fourthSlide = "fourthSection";
            const fifthSlide = "fifthSection";
            const sixthSlide = "sixthSection";
            const seventhSlide = "seventhSection";
            const slide4 = "slide4";
            const slide5 = "slide5";
            const slide6 = "slide6";
            const slide7 = "slide7";
            const slide8 = "slide8";
            const slide9 = "slide9";
            const slide10 = "slide10";
            const slide11 = "slide11";
            const slide12 = "slide12";
            const slide13 = "slide13";
            const slide14 = "slide14";
            const slide15 = "slide15";
            const slide16 = "slide16";
            const slide17 = "slide17";

            const up = "up";
            const down = "down";
            console.log(`ori ${ori},des ${des}, dir ${dir}`);

            switch (dir) {
                //run funstion if slide goes up
                case up:
                    if (ori == secondSlide) {
                        $(".main-of-content").css({
                            top: "0%",
                        });
                        $(".slide-1").css({
                            width: "100vw",
                            height: "100vh",
                        });
                    } else if (ori == thirdSlide) {
                        $(".main-of-content").css({
                            top: "100%",
                        });
                        $(".inner-slide-2").css({
                            width: "100vw",
                            height: "100vh",
                        });
                    } else if (ori == fourthSlide) {
                        $(".main-of-content").css({
                            top: "200%",
                        });
                    } else if (ori == fifthSlide) {
                        $(".main-of-content-2").css({
                            top: "0%",
                        });
                        $(".slide-4 .right-side").css({
                            height: "100vh",
                        });
                    }
                    break;
                case down:
                    //run funstion if slide goes up
                    if (ori == firstSlide) {
                        $(".main-of-content").css({
                            top: "100%",
                        });
                        query_adjust(
                            mobile_query,
                            () => {
                                $(".slide-1").css({
                                    width: "100vw",
                                    height: "28vh",
                                });
                            },
                            () => {
                                $(".slide-1").css({
                                    width: "50vw",
                                    height: "50vh",
                                });
                            }
                        );
                    } else if (ori == secondSlide) {
                        $(".main-of-content").css({
                            top: "200%",
                        });
                        query_adjust(
                            mobile_query,
                            () => {
                                $(".inner-slide-2").css({
                                    width: "100vw",
                                    height: "60vh",
                                });
                            },
                            () => {
                                $(".inner-slide-2").css({
                                    width: "95vw",
                                    height: "60vh",
                                });
                            }
                        );
                    } else if (ori == fourthSlide) {
                        $(".main-of-content-2").css({
                            top: "100%",
                        });
                        $(".slide-4 .right-side").css({
                            height: "0",
                        });
                    } else if (ori == slide11) {
                        setTimeout(function() {
                            $(".sticker1").fadeIn(500);
                        }, 700);
                        setTimeout(function() {
                            $(".sticker2").fadeIn(500);
                        }, 800);
                        setTimeout(function() {
                            $(".sticker3").fadeIn(500);
                        }, 900);
                        setTimeout(function() {
                            $(".sticker4").fadeIn(500);
                        }, 1000);
                        setTimeout(function() {
                            $(".sticker5").fadeIn(500);
                        }, 1100);
                        setTimeout(function() {
                            $(".sticker6").fadeIn(500);
                        }, 1200);
                        setTimeout(function() {
                            $(".sticker7").fadeIn(500);
                        }, 1300);
                        setTimeout(function() {
                            $(".sticker8").fadeIn(500);
                        }, 1400);
                        setTimeout(function() {
                            $(".sticker9").fadeIn(500);
                        }, 1500);
                    }
                    break;
            }
        },
    });
    $(document).on("click", ".e_fake_box", function() {
        fullpage_api.moveTo("slide3", 1);
    });
    $(document).on("click", "#arrow_btn", function() {
        fullpage_api.moveTo("slide2", 1);
    });
    $(document).on("click", "#whatwedo", function() {
        $(".main_menu ul li a").removeClass("active");
        $(this).addClass("active");
        $(".main_menu").hide();
        fullpage_api.moveTo("slide4", 1);
    });
    $(document).on("click", "#ourtechsuite", function() {
        $(".main_menu ul li a").removeClass("active");
        $(this).addClass("active");
        $(".main_menu").hide();
        fullpage_api.moveTo("slide11", 1);
    });
    $(document).on("click", "#yourhole", function() {
        $(".main_menu ul li a").removeClass("active");
        $(this).addClass("active");
        $(".main_menu").hide();
        fullpage_api.moveTo("slide15", 1);
    });
    $(document).on("click", "#tfdyk", function() {
        $(".main_menu ul li a").removeClass("active");
        $(this).addClass("active");
        $(".main_menu").hide();
        fullpage_api.moveTo("slide16", 1);
    });
    $(document).on("click", "#contact_btn", function() {
        fullpage_api.moveTo("slide15", 1);
    });
    </script>
    <script type="text/javascript" src="{{ asset('web/js/main.js') }}?v=1.0.1"></script>
    <script>
    $(window).bind("load", function() {
        $("#fullpage, .menu_box_front").show();
        $(".loader").hide();
    });
    </script>
    <script type="text/javascript">
    var sample = document.getElementById("audio");
    sample.play();

    function tell_me() {
        var v_name = $('#visitor_name');
        if (v_name.val() == '') {
            alert('Please input your name first');
            return;
        }
        var v_email = $('#visitor_email');
        if (v_email.val() == '') {
            alert('Please input your email first');
            return;
        }
        var v_address = $('#visitor_address');
        if (v_address.val() == '') {
            alert('Please input your city first');
            return;
        }
        var v_interest = $('#visitor_interest').val();

        var v_recaptcha = '';
        @if(env('RECAPTCHA_SECRET_KEY_PUBLIC'))
        v_recaptcha = grecaptcha.getResponse();
        if (v_recaptcha.length == 0) {
            // reCaptcha not verified
            alert('Sorry, you need to complete the reCAPTCHA to continue');
            return;
        }
        @endif

        $.ajax({
                type: "POST",
                url: "{{ route('web.tell_me') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    name: v_name.val(),
                    email: v_email.val(),
                    address: v_address.val(),
                    interest: v_interest,
                    recaptcha: v_recaptcha
                },
                beforeSend: function() {
                    // do something before send the data
                    // set default option - just will be selected in "view"
                },
            })
            .done(function(response) {
                // Callback handler that will be called on success
                if (typeof response != 'undefined') {
                    if (response.status == 'true') {
                        // SUCCESS RESPONSE

                        // remove all values in form
                        v_name.val('');
                        v_email.val('');
                        v_address.val('');

                        // close popup
                        hide_popup();

                        setTimeout(function() {
                            alert(response.message);
                        }, 700);
                    } else {
                        // FAILED RESPONSE
                        alert('ERROR: ' + response.message);
                    }
                } else {
                    alert('Server not respond, please try again.');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                // Callback handler that will be called on failure

                // Log the error to the console
                console.error("The following error occurred: " + textStatus, errorThrown);

                alert("The following error occurred: " + textStatus + "\n" + errorThrown);
                // location.reload();
            })
            .always(function() {
                // Callback handler that will be called regardless
                // if the request failed or succeeded
                grecaptcha.reset();
            });
    }

    function request_download() {
        var v_name = $('#request_name');
        if (v_name.val() == '') {
            alert('Please input your full name first');
            return;
        }
        var v_email = $('#request_email');
        if (v_email.val() == '') {
            alert('Please input your email first');
            return;
        }

        $.ajax({
                type: "POST",
                url: "{{ route('web.request_download') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    name: v_name.val(),
                    email: v_email.val(),
                },
                beforeSend: function() {
                    // do something before send the data
                    // set default option - just will be selected in "view"
                },
            })
            .done(function(response) {
                // Callback handler that will be called on success
                if (typeof response != 'undefined') {
                    if (response.status == 'true') {
                        // SUCCESS RESPONSE

                        // remove all values in form
                        v_name.val('');
                        v_email.val('');

                        // close popup
                        hide_popup();

                        setTimeout(function() {
                            let custome_alert = `${response.message}.`
                            console.log(custome_alert)
                            alert(custome_alert);
                        }, 700);
                    } else {
                        // FAILED RESPONSE
                        alert('ERROR: ' + response.message);
                    }
                } else {
                    alert('Server not respond, please try again.');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                // Callback handler that will be called on failure

                // Log the error to the console
                console.error("The following error occurred: " + textStatus, errorThrown);

                alert("The following error occurred: " + textStatus + "\n" + errorThrown);
                // location.reload();
            })
            .always(function() {
                // Callback handler that will be called regardless
                // if the request failed or succeeded
            });
    }
    </script>
</body>

</html>