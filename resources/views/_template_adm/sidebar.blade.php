@php
    // Libraries
    use App\Libraries\Helper;

    $badge_new = '<span class="label label-success pull-right">NEW</span>';
@endphp

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>{{ ucwords(lang('main menu', $translations)) }}</h3>
        <ul class="nav side-menu">
            <li>
                <a href="{{ route('admin.home') }}">
                    <i class="fa fa-home"></i> {{ ucwords(lang('home', $translations)) }}
                </a>
            </li>

            @if (Helper::authorizing('Visitor', 'View List')['status'] == 'true')
                @php
                    $menu_active = '';
                    if(Helper::is_menu_active('/visitor/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.visitor') }}">
                        <i class="fa fa-bar-chart"></i> {{ ucwords(lang('tech suite interest', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Download Cheatsheet', 'View List')['status'] == 'true')
                @php
                    $menu_active = '';
                    if(Helper::is_menu_active('/download-cheatsheet/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.download_cheatsheet') }}">
                        <i class="fa fa-bar-chart"></i> {{ ucwords(lang('download cheat sheet', $translations)) }}
                    </a>
                </li>
            @endif
        </ul>
    </div>

    @php
        $priv_admin = 0;
    @endphp
    <div class="menu_section" id="navmenu_admin" style="display:none">
        <hr>
        <h3>{{ ucwords(lang('administration', $translations)) }}</h3>
        <ul class="nav side-menu">
            @if (Helper::authorizing('System Logs', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                @endphp
                <li>
                    <a href="{{ route('admin.system_logs') }}">
                        <i class="fa fa-exchange"></i> {{ ucwords(lang('system logs', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Config', 'View')['status'] == 'true')
                @php
                    $priv_admin++;
                @endphp
                <li>
                    <a href="{{ route('admin.config') }}">
                        <i class="fa fa-gears"></i> {{ ucwords(lang('config', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Module', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/module/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.module') }}">
                        <i class="fa fa-book"></i> {{ ucwords(lang('module', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Rules', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/rules/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.module_rule') }}">
                        <i class="fa fa-gavel"></i> {{ ucwords(lang('rules', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Office', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/office/') || Helper::is_menu_active('/branch/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.office') }}">
                        <i class="fa fa-building"></i> {{ ucwords(lang('office', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Admin Group', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/group/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.group') }}">
                        <i class="fa fa-users"></i> {{ ucwords(lang('admin group', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Administrator', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/administrator/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.user_admin') }}">
                        <i class="fa fa-user-secret"></i> {{ ucwords(lang('administrator', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Country', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/country/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.country') }}">
                        <i class="fa fa-flag"></i> {{ ucwords(lang('country', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Phrase', 'View List')['status'] == 'true')
                @php
                    $priv_admin++;
                    $menu_active = '';
                    if(Helper::is_menu_active('/phrase/')){
                        $menu_active = 'current-page';
                    }
                @endphp
                <li class="{{ $menu_active }}">
                    <a href="{{ route('admin.phrase') }}">
                        <i class="fa fa-book"></i> {{ ucwords(lang('phrase', $translations)) }}
                    </a>
                </li>
            @endif

            @if (Helper::authorizing('Phrase', 'View')['status'] == 'true')
                <li>
                    <a href="{{ route('dev.cheatsheet_form') }}">
                        <i class="fa fa-file-text-o"></i> {{ ucwords(lang('cheatsheet form', $translations)) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('dev.encrypt') }}">
                        <i class="fa fa-lock"></i> {{ ucwords(lang('encrypt tool', $translations)) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('dev.decrypt') }}">
                        <i class="fa fa-unlock"></i> {{ ucwords(lang('decrypt tool', $translations)) }}
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
<!-- /sidebar menu -->

@section('script-sidebar')
    <script>
        @if ($priv_admin > 0)
            $('#navmenu_admin').show();
        @endif
    </script>
@endsection