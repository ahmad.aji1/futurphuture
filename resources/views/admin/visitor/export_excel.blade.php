<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Interest</th>
        <th>IP Address</th>
        <th>Created (UTC)</th>
        <th>Last Updated (UTC)</th>
    </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach($data as $item)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->address }}</td>
                <td>{{ $item->interest }}</td>
                <td>{{ $item->ip_address }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->updated_at }}</td>
            </tr>
            @php
                $i++;
            @endphp
        @endforeach
    </tbody>
</table>