@extends('_template_adm.master')

@php
    $pagetitle = ucwords(lang('application configuration', $translations));
@endphp

@section('title', $pagetitle)

@section('content')
    <div class="">
        {{-- display response message --}}
        @include('_template_adm.message')

        <div class="page-title">
            <div class="title_left">
                <h3>{{ $pagetitle }}</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ ucwords(lang('form details', $translations)) }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form class="form-horizontal form-label-left" action="{{ route('admin.config') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            @php
                                // set_input_form($type, $input_name, $label_name, $data, $errors, $required = false, $config = null)
                                echo set_input_form('text', 'app_name', ucwords(lang('application name', $translations)), $data, $errors, true);
                                echo set_input_form('text', 'app_version', ucwords(lang('application version', $translations)), $data, $errors, true);
                                echo set_input_form('number', 'app_copyright_year', ucwords(lang('application copyright year', $translations)), $data, $errors, true);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;Input base URL for this application/website that managed by this CMS.';
                                echo set_input_form('text', 'app_url_site', ucwords(lang('application URL', $translations)), $data, $errors, true, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;Input Main URL, if this application used for manage microsite. <br>(If this is used by "blog.your-domain.com" as microsite, then input "your-domain.com" as Main URL)';
                                echo set_input_form('text', 'app_url_main', ucwords(lang('main application URL', $translations)), $data, $errors, false, $config);
                                
                                $config = new \stdClass();
                                $config->info = '<i class="fa fa-info-circle"></i> &nbsp;Recommended to use the smallest image size.';
                                if (empty($data->app_favicon)) {
                                    echo set_input_form('image', 'app_favicon', 'favicon', $data, $errors, true, $config);
                                } else {
                                    echo set_input_form('image', 'app_favicon', 'favicon', $data, $errors, false, $config);
                                }

                                $config = new \stdClass();
                                $config->popup = true;
                                $config->info = '<i class="fa fa-info-circle"></i> &nbsp;Recommended to use the square image, PNG transparent, min. 72 x 72px.';
                                if (empty($data->app_logo)) {
                                    echo set_input_form('image', 'app_logo', ucwords(lang('application logo image', $translations)), $data, $errors, true, $config);
                                } else {
                                    echo set_input_form('image', 'app_logo', ucwords(lang('application logo image', $translations)), $data, $errors, false, $config);
                                }

                                $skins = ['default','festive_yellow', 'feminine_purple', 'racing_red', 'calm_blue', 'simple_black', 'manly_maroon'];
                                $defined_data_skins = [];
                                foreach ($skins as $skin) {
                                    $obj_option = new \stdClass();
                                    $obj_option->id = $skin;
                                    $obj_option->name = ucwords(str_replace('_', ' ', $skin));
                                    $defined_data_skins[] = $obj_option;
                                }
                                $config = new \stdClass();
                                $config->defined_data = $defined_data_skins;
                                $config->placeholder = '- '.ucwords(lang('please choose one', $translations)).' -';
                                echo set_input_form('select2', 'app_skin', ucwords(lang('application theme', $translations)), $data, $errors, true, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;Enter a short description as Info popup content.';
                                $config->autosize = true;
                                echo set_input_form('textarea', 'app_info', ucwords(lang('info', $translations)), $data, $errors, true, $config);

                                echo set_input_form('text', 'powered_by', ucwords(lang('powered by', $translations)), $data, $errors, false);
                                echo set_input_form('text', 'powered_by_url', ucwords(lang('powered by URL', $translations)), $data, $errors, false);
                            @endphp
                            
                            <div class="ln_solid"></div>

                            @php
                                echo set_input_form('text', 'meta_title', 'meta title', $data, $errors, true);

                                $config = new \stdClass();
                                $config->autosize = true;
                                echo set_input_form('textarea', 'meta_description', 'meta description', $data, $errors, true, $config);
                                echo set_input_form('tags', 'meta_keywords', 'meta keywords', $data, $errors, true);
                                echo set_input_form('text', 'meta_author', 'meta author', $data, $errors, true);
                            @endphp

                            <div class="ln_solid"></div>

                            @php
                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;Example: website/article/etc. Read more on <a href="https://ogp.me/#types" target="_blank" style="font-style:italic; text-decoration:underline;">ogp.me <i class="fa fa-external-link"></i></a>.';
                                echo set_input_form('text', 'og_type', 'open graph type', $data, $errors, true, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;If your object is part of a larger web site, the name which should be displayed for the overall site. e.g., "IMDb".';
                                echo set_input_form('text', 'og_site_name', 'open graph site name', $data, $errors, true, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;The title of your object as it should appear within the graph.';
                                echo set_input_form('text', 'og_title', 'open graph title', $data, $errors, true, $config);

                                $config = new \stdClass();
                                $config->popup = true;
                                $config->info = '<i class="fa fa-info-circle"></i> &nbsp;An image which should represent your object within the graph.';
                                if (empty($data->og_image)) {
                                    echo set_input_form('image', 'og_image', 'open graph image', $data, $errors, true, $config);
                                } else {
                                    echo set_input_form('image', 'og_image', 'open graph image', $data, $errors, false, $config);
                                }

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;A one to two sentence description of your object.';
                                $config->autosize = true;
                                echo set_input_form('textarea', 'og_description', 'open graph description', $data, $errors, true, $config);
                            @endphp

                            <div class="ln_solid"></div>

                            @php
                                $config = new \stdClass();
                                $config->defined_data = ["summary", "summary_large_image", "app", "player"];
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;You can check <a href="https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/markup" target="_blank" style="font-style:italic; text-decoration:underline;">Twitter Dev Docs <i class="fa fa-external-link"></i></a> for more details. And test your Twitter Card on <a href="https://cards-dev.twitter.com/validator" target="_blank" style="font-style:italic; text-decoration:underline;">Card Validator <i class="fa fa-external-link"></i></a>.';
                                echo set_input_form('select', 'twitter_card', 'Twitter Card', $data, $errors, true, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;@username for the website used in the card footer.';
                                echo set_input_form('text', 'twitter_site', 'Twitter Site', $data, $errors, false, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;Same as Twitter Site, but the user’s Twitter ID. You can use <a href="https://tweeterid.com/" target="_blank" style="font-style:italic; text-decoration:underline;">tweeterid.com <i class="fa fa-external-link"></i></a> to get Twitter ID.';
                                echo set_input_form('text', 'twitter_site_id', 'Twitter Site ID', $data, $errors, false, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;@username for the content creator/author.';
                                echo set_input_form('text', 'twitter_creator', 'Twitter Creator', $data, $errors, false, $config);

                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;Twitter user ID of content creator.';
                                echo set_input_form('text', 'twitter_creator_id', 'Twitter Creator ID', $data, $errors, false, $config);
                            @endphp
                            
                            <div class="ln_solid"></div>

                            @php
                                $config = new \stdClass();
                                $config->info_text = '<i class="fa fa-info-circle"></i> &nbsp;In order to use Facebook Insights you must add the app ID to your page. Insights lets you view analytics for traffic to your site from Facebook. Read more on <a href="https://developers.facebook.com/docs/sharing/webmasters/" target="_blank" style="font-style:italic; text-decoration:underline;">FB Dev Docs <i class="fa fa-external-link"></i></a>. And test your markup on <a href="https://developers.facebook.com/tools/debug/" target="_blank" style="font-style:italic; text-decoration:underline;">Sharing Debugger <i class="fa fa-external-link"></i></a>.';
                                echo set_input_form('text', 'fb_app_id', 'FB App ID', $data, $errors, false, $config);
                            @endphp

                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; 
                                        @if (isset($data))
                                            {{ ucwords(lang('save', $translations)) }}
                                        @else
                                            {{ ucwords(lang('submit', $translations)) }}
                                        @endif
                                    </button>
                                    <a href="{{ route('admin.config') }}" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp; {{ ucwords(lang('cancel', $translations)) }}</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- Switchery -->
    @include('_vendors.switchery.css')
    <!-- PhotoSwipe -->
    @include('_vendors.photoswipe.css')
@endsection

@section('script')
    <!-- Switchery -->
    @include('_vendors.switchery.script')
    <!-- autosize -->
    @include('_vendors.autosize.script')
    <!-- jQuery Tags Input -->
    @include('_vendors.tagsinput.script')
    <!-- PhotoSwipe -->
    @include('_vendors.photoswipe.script')
@endsection