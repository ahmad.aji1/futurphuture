<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class visitor_download extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'visitors_download_cheatsheet';
}
