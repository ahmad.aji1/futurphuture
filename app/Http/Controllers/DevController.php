<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

// MAIL
use App\Mail\MailTester;

// Libraries
use App\Libraries\Helper;

class DevController extends Controller
{
    public function sandbox()
    {
        // $string = 'sudo123!';
        // echo Helper::hashing_this($string);

        // Helper::logging(1);
        // echo date_default_timezone_get();

        // Current date/time in the specified time zone.
        // $date = new \DateTime(null, new \DateTimeZone('Asia/Jakarta'));
        // echo $date->format('Y-m-d H:i:s P') . "\n";

        // echo Helper::current_datetime('Y-m-d H:i:s P', 'Asia/Singapore');
        echo Helper::current_datetime('Y-m-d H:i:s');
    }

    public function cheatsheet_form()
    {
        return view('admin.core.dev.cheatsheet_form');
    }

    public function encrypt(Request $request)
    {
        if ($request->isMethod('post')) {
            $dir_path = 'uploads/tmp/';
            $file = $request->file('key');
            $uploaded_file = Helper::upload_file($dir_path, $file, true, null, ['txt']);
            if ($uploaded_file['status'] == 'false') {
                return back()
                    ->withInput()
                    ->with('error', $uploaded_file['message']);
            }
            $uploaded_file_name = $uploaded_file['data'];

            $result = Helper::encrypt($request->string, 'public/' . $dir_path . $uploaded_file_name);

            // remove the uploaded key file
            $uploaded_file_path = public_path($dir_path . $uploaded_file_name);
            if (file_exists($uploaded_file_path)) {
                unlink($uploaded_file_path);
            }

            $data = new \stdClass();
            $data->string = $request->string;
            $data->result = $result;

            return view('admin.core.dev.encrypt', compact('data'));
        } else {
            return view('admin.core.dev.encrypt');
        }
    }

    public function decrypt(Request $request)
    {
        if ($request->isMethod('post')) {
            $dir_path = 'uploads/tmp/';
            $file = $request->file('key');
            $uploaded_file = Helper::upload_file($dir_path, $file, true, null, ['txt']);
            if ($uploaded_file['status'] == 'false') {
                return back()
                    ->withInput()
                    ->with('error', $uploaded_file['message']);
            }
            $uploaded_file_name = $uploaded_file['data'];

            $result = Helper::decrypt($request->string, 'public/' . $dir_path . $uploaded_file_name);

            // remove the uploaded key file
            $uploaded_file_path = public_path($dir_path . $uploaded_file_name);
            if (file_exists($uploaded_file_path)) {
                unlink($uploaded_file_path);
            }

            $data = new \stdClass();
            $data->string = $request->string;
            $data->result = $result;

            return view('admin.core.dev.decrypt', compact('data'));
        } else {
            return view('admin.core.dev.decrypt');
        }
    }

    /**
     * EMAIL
     */
    public function email_send(Request $request)
    {
        // SET THE DATA
        $data = \App\Models\admin::first();

        // SET EMAIL SUBJECT
        $subject_email = 'Test Send Email from ' . env('APP_NAME');

        $email_address = $request->email;
        if ($request->send && !$email_address) {
            return 'Must set email as recipient in param email';
        }

        try {
            // SEND EMAIL
            if ($request->send) {
                // send email using SMTP
                Mail::to($email_address)->send(new MailTester($data, $subject_email));
            } else {
                // rendering email in browser
                return (new MailTester($data, $subject_email))->render();
            }
        } catch (\Exception $e) {
            // Debug via $e->getMessage();
            dd($e->getMessage());
            // return "We've got errors!";
        }

        return 'Successfully sent email to ' . $email_address;
    }
}
