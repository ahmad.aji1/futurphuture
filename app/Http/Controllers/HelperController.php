<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

// Libraries
use App\Libraries\Helper;

class HelperController extends Controller
{
    public function filter_district(Request $request)
    {
        $validation = [
            'parent' => 'required'
        ];
        $message = [
            'required' => ':attribute ' . lang('should not be empty', $this->translations),
        ];
        $names = [
            'parent' => ucwords(lang('parent', $this->translations))
        ];
        $validator = Validator::make($request->all(), $validation, $message, $names);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'false',
                'message' => implode(', ', $validator->errors()->all()),
                'status' => $validator->errors()->messages(),
            ]);
        }

        $parent = Helper::validate_input($request->parent);

        $data = DB::table('id_cities')
            ->select(
                'city_code AS kode',
                'city_name AS nama'
            )
            ->where('city_province_code', $parent)
            ->orderBy('city_name')
            ->get();

        $response = [
            'status' => 'true',
            'message' => 'Successfully get data',
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function filter_sub_district(Request $request)
    {
        $parent = Helper::validate_input($request->parent);

        $data = DB::table('id_sub_districts')
            ->select(
                'sub_district_code AS kode',
                'sub_district_name AS nama'
            )
            ->where('sub_district_city_code', $parent)
            ->orderBy('sub_district_name')
            ->get();

        $response = [
            'status' => 'true',
            'message' => 'Successfully get data',
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function filter_village(Request $request)
    {
        $parent = Helper::validate_input($request->parent);

        $data = DB::table('id_villages')
            ->select(
                'village_code AS kode',
                'village_name AS nama'
            )
            ->where('village_sub_district_code', $parent)
            ->orderBy('village_name')
            ->get();

        $response = [
            'status' => 'true',
            'message' => 'Successfully get data',
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function filter_postal_code(Request $request)
    {
        $parent = Helper::validate_input($request->parent);

        $query = DB::table('id_villages')
            ->where('village_code', $parent)
            ->first();

        $data = [];
        if ($query) {
            $array = explode(',', $query->village_postal_codes);
            if (isset($array[0])) {
                foreach ($array as $item) {
                    $obj = new \stdClass();
                    $obj->kode = $item;
                    $obj->nama = $item;
                    $data[] = $obj;
                }
            }
        }

        $response = [
            'status' => 'true',
            'message' => 'Successfully get data',
            'data' => $data
        ];
        return response()->json($response, 200);
    }
}
