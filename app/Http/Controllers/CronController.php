<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Libraries
use App\Libraries\Helper;

// Models
use App\Models\credit_card_bill;
use App\Models\admin_notification;

class CronController extends Controller
{
    public function cc_bill_payment_reminder()
    {
        // cek apakah ada tagihan yg blm dibayar atau msh ada sisa pembayaran selama periode

        // set periode (hari ini + 7 hari)
        $date = date_create(date('Y-m-d'));
        date_add($date, date_interval_create_from_date_string("7 days"));
        $periods = date_format($date, "Y-m-d");

        $bills = credit_card_bill::select(
            'credit_card_bills.*',
            'credit_cards.ownership_type',
            'credit_cards.owner_name',
            'credit_cards.card_type',
            'banks.name AS bank'
        )
            ->leftJoin('credit_cards', 'credit_card_bills.credit_card_id', 'credit_cards.id')
            ->leftJoin('banks', 'credit_cards.bank_id', 'banks.id')
            ->where(function ($query_where) {
                $query_where->where('credit_card_bills.total_paid', 0)
                    ->orWhere('credit_card_bills.remaining_bill', '>', 0);
            })
            ->whereDate('due_date', '<', $periods)
            ->get();

        if (isset($bills[0])) {
            DB::beginTransaction();
            try {
                // jika ada tagihannya, maka push notif
                foreach ($bills as $item) {
                    $data = new admin_notification();
                    $data->admin_id = 65; // superuser
                    $data->module_id = 13; // credit card bill
                    $data->target_id = $item->id;
                    if ($item->remaining_bill > 0) {
                        // Segera lunasi tagihan CC bank
                        $data->subject = 'Pay off ' . $item->bank . ' CC bill immediately';
                        // Lunasi tagihan CC bank sebesar Rp xxx (dari total Rp xxx) sebelum tanggal jatuh tempo pada xxx.
                        $data->content = 'Pay off ' . $item->bank . ' CC bill of IDR ' . number_format($item->remaining_bill) . ' (from total IDR ' . number_format($item->total_bill) . ') before the due date on ' . date('D, d M Y', strtotime($item->due_date)) . '.';
                    } else {
                        // Segera bayar tagihan CC bank
                        $data->subject = 'Pay ' . $item->bank . ' CC bill immediately';
                        // Bayar tagihan CC bank sebesar Rp xxx sebelum tanggal jatuh tempo pada xxx.
                        $data->content = 'Pay ' . $item->bank . ' CC bill of IDR ' . number_format($item->total_bill) . ' before the due date on ' . date('D, d M Y', strtotime($item->due_date)) . '.';
                    }
                    $data->clickable = 1;
                    $data->read_status = 0;
                    $data->save();
                }

                // TODO send email reminder

                DB::commit();

                return 'OK';
            } catch (\Exception $ex) {
                DB::rollback();

                $error_msg = $ex->getMessage() . ' in ' . $ex->getFile() . ' at line ' . $ex->getLine();
                Helper::error_logging($error_msg, null, null, 'CRON CC Bill Payment Reminder');

                # ERROR
                return $error_msg;
            }
        }
    }
}
