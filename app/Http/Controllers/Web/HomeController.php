<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

// MAIL
use App\Mail\MailCheatsheet;
use App\Mail\MailInterest;

// Libraries
use App\Libraries\Helper;

// Models
use App\Models\visitor;
use App\Models\visitor_download;

class HomeController extends Controller
{
    /**
     * Display home page.
     *
     * @return view()
     */
    public function index()
    {
        return view('web.home');
    }

    public function tell_me(Request $request)
    {
        $validation = [
            'name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'interest' => 'required',
        ];
        $message = [
            'required' => ':attribute ' . lang('should not be empty', $this->translations),
            'email' => ':attribute ' . lang('must be a valid email address', $this->translations),
        ];
        $names = [
            'name' => ucwords(lang('name', $this->translations)),
            'email' => ucwords(lang('email', $this->translations)),
            'address' => ucwords(lang('address', $this->translations)),
            'interest' => ucwords(lang('interest', $this->translations)),
        ];
        $validator = Validator::make($request->all(), $validation, $message, $names);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'false',
                'message' => implode(', ', $validator->errors()->all()),
                'data' => $validator->errors()->messages(),
            ]);
        }

        if (env('RECAPTCHA_SECRET_KEY_PUBLIC')) {
            // reCAPTCHA checking...
            $recaptcha_check = Helper::validate_recaptcha($request->recaptcha, env('RECAPTCHA_SECRET_KEY_PUBLIC'));
            if (!$recaptcha_check) {
                # ERROR - reCAPTCHA FAILED
                return response()->json([
                    'status' => 'false',
                    'message' => lang('reCAPTCHA validation unsuccessful, please try again', $this->translations),
                    'data' => '',
                ]);
            }
        }

        DB::beginTransaction();
        try {

            // INSERT NEW DATA
            $data = new visitor();

            // HELPER VALIDATION FOR PREVENT SQL INJECTION & XSS ATTACK
            $name = Helper::validate_input_text($request->name);
            if (!$name) {
                return response()->json([
                    'status' => 'false',
                    'message' => lang('Invalid value for #item', $this->translations, ['#item' => $names['name']]),
                    'data' => '',
                ]);
            }
            $data->name = strtoupper($name);

            $email = Helper::validate_input_email($request->email);
            if (!$email) {
                return response()->json([
                    'status' => 'false',
                    'message' => lang('Invalid value for #item', $this->translations, ['#item' => $names['email']]),
                    'data' => '',
                ]);
            }
            $data->email = strtoupper($email);

            $address = Helper::validate_input_text($request->address);
            if (!$address) {
                return response()->json([
                    'status' => 'false',
                    'message' => lang('Invalid value for #item', $this->translations, ['#item' => $names['address']]),
                    'data' => '',
                ]);
            }
            $data->address = strtoupper($address);

            $interest = Helper::validate_input_text($request->interest);
            if (!in_array($interest, ['ALL', 'KUANTICO', 'RONUT', 'SKYYWALKER', 'TRIP', 'AMES', 'SUPA', 'GEESTACKE'])) {
                return response()->json([
                    'status' => 'false',
                    'message' => lang('Invalid value for #item', $this->translations, ['#item' => $names['interest']]),
                    'data' => '',
                ]);
            }
            $data->interest = strtoupper($interest);

            $data->ip_address = $request->ip();

            $data->save();

            DB::commit();

            // SEND EMAIL
            $subject_email = "Someone is interested in " . $interest;

            // rendering email in browser
            // return (new MailInterest($data, $subject_email))->render();

            $internal_email_json = env('INTERNAL_EMAIL');
            if ($internal_email_json) {
                $recipients = json_decode($internal_email_json);
                if (is_array($recipients)) {
                    foreach ($recipients as $email_address) {
                        // send email using SMTP
                        Mail::to($email_address)->send(new MailInterest($data, $subject_email));
                    }
                }
            }

            $response = [
                'status' => 'true',
                'message' => lang('Got your ask… brb!', $this->translations),
                'data' => ''
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            DB::rollback();

            $error_msg = $ex->getMessage() . ' in ' . $ex->getFile() . ' at line ' . $ex->getLine();
            Helper::error_logging($error_msg, null, null, 'Web: Tell Me Now');

            if (env('APP_DEBUG') == false) {
                $error_msg = lang('Oops, sorry we failed to tell you about #item. Please try again.', $this->translations, ['#item' => $interest]);
            }

            # ERROR
            return response()->json([
                'status' => 'false',
                'message' => $error_msg,
                'data' => '',
            ]);
        }
    }

    public function request_download(Request $request)
    {
        $validation = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $message = [
            'required' => ':attribute ' . lang('should not be empty', $this->translations),
            'email' => ':attribute ' . lang('must be a valid email address', $this->translations),
        ];
        $names = [
            'name' => ucwords(lang('name', $this->translations)),
            'email' => ucwords(lang('email', $this->translations)),
        ];
        $validator = Validator::make($request->all(), $validation, $message, $names);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'false',
                'message' => implode(', ', $validator->errors()->all()),
                'data' => $validator->errors()->messages(),
            ]);
        }

        DB::beginTransaction();
        try {

            // INSERT NEW DATA
            $data = new visitor_download();

            // HELPER VALIDATION FOR PREVENT SQL INJECTION & XSS ATTACK
            $name = Helper::validate_input_text($request->name);
            if (!$name) {
                return response()->json([
                    'status' => 'false',
                    'message' => lang('Invalid value for #item', $this->translations, ['#item' => $names['name']]),
                    'data' => '',
                ]);
            }
            $data->name = strtoupper($name);

            $email = Helper::validate_input_email($request->email);
            if (!$email) {
                return response()->json([
                    'status' => 'false',
                    'message' => lang('Invalid value for #item', $this->translations, ['#item' => $names['email']]),
                    'data' => '',
                ]);
            }
            $data->email = strtoupper($email);

            $data->ip_address = $request->ip();

            $data->save();

            // generate token to identify by who the link has been accessed
            $token = Helper::generate_token($data->id);
            $data->token = $token;

            DB::commit();

            // SEND EMAIL
            $email_address = $email;
            $subject_email = "[SUPERFUTUREKIDS] Here's Your Cheat Sheet";

            // send email using SMTP
            Mail::to($email_address)->send(new MailCheatsheet($data, $subject_email));

            // rendering email in browser
            // return (new MailCheatsheet($data, $subject_email))->render();

            $response = [
                'status' => 'true',
                'message' => lang('Mr. Postman will be dropping by your mailbox soon..', $this->translations),
                'data' => ''
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            DB::rollback();

            $error_msg = $ex->getMessage() . ' in ' . $ex->getFile() . ' at line ' . $ex->getLine();
            Helper::error_logging($error_msg, null, null, 'Web: Tell Me Now');

            if (env('APP_DEBUG') == false) {
                $error_msg = lang('Oops, sorry we failed to send full cheat sheet to your email. Please try again.', $this->translations);
            }

            # ERROR
            return response()->json([
                'status' => 'false',
                'message' => $error_msg,
                'data' => '',
            ]);
        }
    }

    public function view_cheatsheet(Request $request)
    {
        $token = $request->by;
        if ($token) {
            $id = (int) Helper::validate_token($token);
            $data = visitor_download::find($id);
            if ($data) {
                $data->total_accessed += 1;
                $data->save();
            }
        }

        return redirect(asset('uploads/SUPERFK-cheatsheet.pdf'));
    }
}
