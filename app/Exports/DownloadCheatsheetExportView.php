<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

// MODELS
use App\Models\visitor_download;

class DownloadCheatsheetExportView implements FromView
{
    use Exportable;

    public function __construct()
    {
        $this->data = $this->get_data();
    }

    private function get_data()
    {
        // GET THE DATA
        $query = visitor_download::orderBy('id', 'desc');

        $data = $query->get();

        return $data;
    }

    public function view(): View
    {
        return view('admin.download_cheatsheet.export_excel', [
            'data' => $this->data
        ]);
    }
}
